---
title: "Curriculum Vitae"
Description: "Making the Complex Simple and Easy to Understand!"
layout: "CV"
tags: [cv, resumee]
---

&nbsp;

## Education

- **Ph.D.** High Energy Theoretical Physics, _University of Goettingen (DE)_ - September 2022
    - Higher Order Electroweak and QCD Corrections in $e^+e^- \mu^+\mu^-$ production at LHC (not yet public)

- **Master** Theretical Physics, _University of Bologna (IT)_ - December 2018
   - [Comparison between two different approaches to resum large threshold logarithms in Drell-Yan rapidity distribution](https://amslaurea.unibo.it/17102/1/Villani_Tesi.pdf)

- **Bachelor** Physics, _University of Bologna (IT)_ - October 2015
    - [Path Integral and Weyl Ordering](https://amslaurea.unibo.it/9443/1/Integrali_sui_Cammini_e_Ordinamento_di_Weyl.pdf)

## Scientific Publications

- _[Higher-order EW corrections in $ZZ$ and $ZZj$ production at the LHC](https://inspirehep.net/files/74ef5af11dcb1e3874cb4b63d6d29f97)_ - JHEP 06 (2022) 064

- _[A study of loop-induced $ZH$ production with up to one additional jet](https://arxiv.org/pdf/2003.01700.pdf)_ - PhysTeV (2019)


## Scientific Presentations

- 14th Annual Meeting of the Helmholtz Alliance "Physics at the Terascale" (11/21)
    - [Diboson production including NLO QCD and electroweak corrections](https://indico.desy.de/event/31325/contributions/112662/attachments/70049/89015/Diboson%20production%20including%20NLO%20QCD%20and%20electroweak%20corrections.pdf)

- 22nd MCnet General Meeting (04/21)
    - [Improvements to diboson production through higher order corrections](https://indico.cern.ch/event/1005701/contributions/4316317/contribution.pdf)

- LHC Higgs Cross Section Working Group 1 Meeting (Parton Shower Uncertainties in Higgs Measurements) (04/20)
    - [A study of loop-induced ZH production with up to one additional jet](https://indico.cern.ch/event/904668/contributions/3807022/attachments/2016963/3371316/PS_HXSWG_Meeting.pdf)

- 20nd MCnet General Meeting (04/20)
    - [NLO+PS matching for loop-induced processes in SHERPA](https://indico.cern.ch/event/907278/contributions/3824433/attachments/2030135/3397418/MCnet_1_2020.pdf)

- Sherpa Collaboration General Meeting (01/20)

- 19th MCnet General Meeting (09/19)


## Teaching Activities

- Tutor: **Quantum Field Theory 1** (MSc level course) - _University of Goettingen fall 2020_

- Tutor: **Advanced Quantum Mechanics** (MSc level course) - _University of Goettingen winter 2020_

- Teaching Assistant: **Numerical Methods for Science** (MSc level course) - _University of Goettingen fall 2021_

## Awards & Fellowships

- Doctoral scholarship funded by MCnetITN3 

- National phase of the International Physicist Tournament - _1st Place_ 

- International phase of the International Physicist Tournament - _10th Place_

## Skills

**Computing**

- Languages: _C++, C, Python, Bash, SQL, HTML, CSS_
- OS: _Linux, MacOS, Windows_
- Data Visualization: _Pandas, Matplot, Rivet, Spread Sheets_
- Tools: _Git, Tensorflow, PyTorch, Numpy, LAPACK, Btwxt, Sherpa_

**Non-physics related** 
- Deep Learning
- Machine Learning
- Statistics
- Quantitative Finance


**Languages**

- Italiano: _Native_
- English: _C1 (IELTS 2020)_
- Deutsch: _B2 (University of Goettingen)_

## Contacts

- **Email** - simon.luca.villani [at] gmail [dot] com
- **LinkedIn** - [Simon Luca Villani](https://www.linkedin.com/in/simon-luca-villani-403abb176/)
- **GitLab** - [Prinoticommmi](https://gitlab.com/Prinoticommmi)
- **Research Gate** - [Simon Luca Villani](https://www.researchgate.net/profile/Simon-Luca-Villani)